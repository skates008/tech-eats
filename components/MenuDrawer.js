import React, { Component } from "react";
import { Text, View, Platform,StyleSheet,Dimensions } from "react-native";

const WIDTH = Dimensions.get("window").width;
const HEIGHT = Dimensions.get("window").height;
export default class MenuDrawer extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text> MenuDrawer</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    paddingTop: 10,
    backgroundColor: "#ecf0f1",
    padding: 8
  }
});
