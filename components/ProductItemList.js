import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  ScrollView,
  ImageBackground,
  TouchableOpacity
} from "react-native";

const width = Dimensions.get("window").width;

export default class ProductItemList extends Component {
  constructor(props) {
    super(props);
    this.addToCart = this.addToCart.bind(this);
  }
  
  addToCart = (e) => {
    e.preventDefault();
    console.log("eeee");
    alert("test cart");
  }

  

  render() {
    var _this= this;
    const menusCollection = this.props.value;
    return (
      <View>
        <ScrollView>
          {menusCollection.map((menu, index) => {
            return (
              <View
                key={index}
                // ref={refs[menu.id]}
                style={{ marginTop: 10 }}
              >
                <Text style={styles.menuListHead}>{menu.name}</Text>
                {menu.subcategory.map(function(sc, indexsc) {
                  return (
                    <View key={indexsc}>
                      <Text style={styles.subCategoryName}>{sc.name}</Text>

                      <View>
                        <View style={styles.itemList}>
                          {sc.menus.map((subMenu, indexsc1) => {
                            return (
                              <View
                                key={indexsc1}
                                style={[styles.box, styles.shadow3]}
                              >
                                <ImageBackground
                                  source={{
                                    uri: subMenu.image
                                  }}
                                  style={styles.imageCard}
                                ></ImageBackground>
                                <Text>{subMenu.title}</Text>
                                <Text style={styles.itemPrice}>
                                  {subMenu.price}QAR
                                </Text>

                                <View style={styles.addToCartView}>
                                  <TouchableOpacity 
                                  onPress={_this.addToCart}>
                                  
                                    <Text style={styles.btnAddCart}>Add</Text>
                                  </TouchableOpacity>
                                </View>
                              </View>
                            );
                          })}
                        </View>
                      </View>
                    </View>
                  );
                })}
              </View>
            );
          })}
        </ScrollView>
      </View>
    );
  }
}

function elevationShadowStyle(elevation) {
  return {
    elevation,
    shadowColor: "black",
    shadowOffset: { width: 0, height: 0.5 * elevation },
    shadowOpacity: 0.3,
    shadowRadius: 0.8 * elevation
  };
}

const styles = StyleSheet.create({
  shadow3: elevationShadowStyle(20),

  box: {
    borderRadius: 8,
    backgroundColor: "white",
    padding: 24,
    width: width / 2 - 15,
    height: "auto",
    marginLeft: 10,
    marginTop: 10
  },

  addToCartView: {
    justifyContent: "flex-end",
    flex: 1,
    flexDirection: "row"
  },
  menuListHead: {
    width: "100%",
    margin: 0,
    backgroundColor: "#005697",
    color: "#fff",
    fontSize: 20,
    paddingTop: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingLeft: 10,
    textTransform: "uppercase"
  },
  subCategoryName: {
    textTransform: "capitalize",
    fontSize: 30
  },
  itemList: {
    flexDirection: "row",
    flexWrap: "wrap"
  },
  imageCard: {
    borderRadius: 128,
    width: 150,
    height: 200
  },
  itemPrice: {
    color: "#808080",
    marginTop: 4
  },
  btnAddCart: {
    color: "#fff",
    backgroundColor: "#005697",
    paddingTop: 3,
    paddingRight: 14,
    paddingBottom: 3,
    paddingLeft: 14,
    borderRadius: 50
  }
});
