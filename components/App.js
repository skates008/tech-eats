import React, { Component } from "react";
import { StyleSheet, Text, View, Button, Picker } from "react-native";
import SearchableDropdown from "react-native-searchable-dropdown";
import Header from "./views/Header";
import {
  createDrawerNavigator,
  createAppContainer,
  DrawerItems,
  SafeAreaView,
} from 'react-navigation';
import { Ionicons } from '@expo/vector-icons';

export default class App extends Component {
  static navigationOptions = {
    title: 'Home',
    drawerIcon: ({ focused }) => (
      <Ionicons name="md-home" size={24} color={focused ? 'blue' : 'black'} />
    ),
  };
  constructor() {
    super();
    this.state = {
      citiesCollection: [],
      locationCollection: []
    };
  }

  onCityChangeEvent = (items) => {
    this.state.locationCollection=[];
    const cities = this.state.citiesCollection;
    if(cities && cities.length>0){
      //this.state.locationCollection = items.child;
      this.setState({
        locationCollection: [...this.state.locationCollection, ...items.child]
      });
    }
  };

  componentDidMount() {
    fetch("http://techeats.co/demotest/api/locations")
      .then(response => response.json())
      .then(responseJson => {
        //console.log("-------r", responseJson);
        this.setState({
          citiesCollection: [...this.state.citiesCollection, ...responseJson]
        });
      })
      .catch(error => {
        console.error(error);
      });
  }

  render() {
    return (
      <View style={styles.container}>
        <Header />
        <Text
          style={styles.paragraph}
          onPress={() => {
            this.props.navigation.toggleDrawer();
          }}>
          Toggle Drawer1
        </Text>
        <Text style={{ marginLeft: 10 }}></Text>

        <SearchableDropdown
          onItemSelect={(item) => this.onCityChangeEvent(item)}
         //onItemSelect={item => alert(JSON.stringify(item))}
          containerStyle={{ padding: 5 }}
          textInputStyle={{
            padding: 12,
            borderWidth: 1,
            borderColor: "#ccc",
            backgroundColor: "#FAF7F6"
          }}
          itemStyle={{
            padding: 10,
            marginTop: 2,
            backgroundColor: "#FAF9F8",
            borderColor: "#bbb",
            borderWidth: 1
          }}
          itemTextStyle={{
            color: "#222"
          }}
          itemsContainerStyle={{
            maxHeight: "50%"
          }}
          items={this.state.citiesCollection}
          defaultIndex={2}
          placeholder="Enter City"
          resetValue={false}
           
          //reset textInput Value with true and false state
          underlineColorAndroid="transparent"
          //To remove the underline from the android input
        />

<SearchableDropdown
        //  onItemSelect={(item) => this.onCityChangeEvent(item)}
         //onItemSelect={item => alert(JSON.stringify(item))}
          containerStyle={{ padding: 5 }}
          textInputStyle={{
            padding: 12,
            borderWidth: 1,
            borderColor: "#ccc",
            backgroundColor: "#FAF7F6"
          }}
          itemStyle={{
            padding: 10,
            marginTop: 2,
            backgroundColor: "#FAF9F8",
            borderColor: "#bbb",
            borderWidth: 1
          }}
          itemTextStyle={{
            color: "#222"
          }}
          itemsContainerStyle={{
            maxHeight: "50%"
          }}
          items={this.state.locationCollection}
          defaultIndex={2}
          placeholder="Enter City"
          resetValue={false}
           
          //reset textInput Value with true and false state
          underlineColorAndroid="transparent"
          //To remove the underline from the android input
        />
      </View>
    );
  }
}

const CustomDrawerContentComponent = props => (
  <ScrollView>
    <SafeAreaView
      style={styles.container}
      forceInset={{ top: 'always', horizontal: 'never' }}>
      <DrawerItems {...props} />
      <Image
        style={styles.image}
        source={{
          uri: 'https://appjs.co/wp-content/uploads/2015/09/brent3-458x458.png',
        }}
      />
    </SafeAreaView>
  </ScrollView>
);

const navigator = createDrawerNavigator(
  {
    Home,
    Profile,
  },
  {
    // drawerType: 'back',
    // drawerPosition: 'right',
    // drawerWidth: 200,
    // drawerBackgroundColor: 'orange',
    // contentComponent: CustomDrawerContentComponent
  }
);

export default createAppContainer(navigator);

const styles = StyleSheet.create({
  container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center'
  }
});