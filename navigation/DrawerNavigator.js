import React from "react";
import { StyleSheet, Platform, Dimensions } from "react-native";
import { createAppContainer } from "react-navigation";
import { createDrawerNavigator } from "react-navigation-drawer";
import { createStackNavigator } from "react-navigation-stack";
import { Ionicons, FontAwesome, Entypo } from "@expo/vector-icons";

import Landing from "../views/Landing";
import Order from "../views/Order";
import Login from "../views/Login";
import Home from "../views/Home";
import Checkout from "../views/Checkout";
import MyOrder from "../views/MyOrder";
const WIDTH = Dimensions.get("window").width;

const DrawerConfig = {
  initialRouteName: "Landing",
  drawerWidth: WIDTH * 0.83,
  drawerPosition: "left"
};

const navigationOptions = {
  
  headerStyle: {
    backgroundColor: "#f4511e"
  },
  headerTintColor: "#fff",
  headerTitleStyle: {
    fontWeight: "bold"
  }
}
leftHeader= (navigation) =>{
  return(
  
    <Ionicons
      name="md-menu"
      color="#000000"
      size={32}
      style={styles.menuIcon}
      onPress={() => navigation.toggleDrawer()}
    />
  )
 
}
const HomeStack = createStackNavigator(
  {
    Home: {
      screen: Home
    }
  },
  {
    defaultNavigationOptions: ({ navigation }) => {
      return {
        headerLeft: this.leftHeader(navigation),
        headerStyle: {
          backgroundColor: "#f4511e"
        },
        headerTintColor: "#fff",
        headerTitleStyle: {
          fontWeight: "bold"
        }
      };
    }
  }
);

const LandingStack = createStackNavigator({
  Landing: { screen: Landing },
  Order: { screen: Order },
  Checkout: { screen: Checkout }
},
{
  defaultNavigationOptions: ({ navigation }) => {
    return {
      headerLeft: this.leftHeader(navigation),
      navigationOptions
    };
  }
}
);

const OrderStack = createStackNavigator(
  {
    Order: {
      screen: Order
    }
  },
  {
    defaultNavigationOptions: ({ navigation }) => {
      return {
        headerLeft: this.leftHeader(navigation), 
        navigationOptions
      };
    }
  }
);

const LoginStack = createStackNavigator(
  {
    Login: {
      screen: Login
    }
  },
  {
    defaultNavigationOptions: ({ navigation }) => {
      return {
        headerLeft: this.leftHeader(navigation),
        navigationOptions
      };
    }
  }
);

const MyOrderStack = createStackNavigator(
  {MyOrder: {screen: MyOrder}},
  {defaultNavigationOptions: ({navigation})=> {
    return {
      headerLeft: this.leftHeader(navigation),
      navigationOptions
    };
    }}
);

const MainDrawer = createDrawerNavigator({
  // Home: {
  //   screen: HomeStack
  // },
  //headerTitle 
  Landing: {
    screen: LandingStack,
    navigationOptions: () => ({
      drawerIcon: ({ focused }) => (
        <Ionicons name="md-person" size={24} color={focused ? "blue" : "black"} />
      )
    }),  
   
      
  },
  Order: {
    screen: OrderStack,
    navigationOptions: () => ({
      title: 'Order Online',
      drawerIcon: ({ focused }) => (
        <Ionicons
          name="ios-bookmarks"
          size={24}
          color={focused ? "blue" : "black"}
        />
      )
    }),  
  },
  Login: {
    screen: LoginStack,
    navigationOptions: () => ({
      drawerIcon: ({ focused }) => (
        <Entypo name="login" size={24} color={focused ? "blue" : "black"} />
      )
    }),  
  },
  MyOrder: {
    screen: MyOrderStack,
    navigationOptions: () => ({
      title: 'My Orders',
      drawerIcon: ({focused}) => (
          <Entypo name="back-in-time" size={24} color={focused ? "blue" : "black"} />
      )
    })
  }
});

const RootStack = createStackNavigator({
  MainDrawer: {
    screen: MainDrawer
  }
});
const styles = StyleSheet.create({
  menuIcon: {
    marginLeft: 20
  },

  shoppingCartIcon: {
    marginRight: 30
  }
});
export default createAppContainer(MainDrawer);
