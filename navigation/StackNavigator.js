import React, { Component } from "react";
import { StyleSheet, Platform, Dimensions } from "react-native";
import { createAppContainer,DrawerActions } from "react-navigation";

import { createStackNavigator } from "react-navigation-stack";
import { Ionicons } from "@expo/vector-icons";

import Landing from "../views/Landing";
import Order from "../views/Order";
import Login from "../views/Login";
import Home from "../views/Home";

const WIDTH = Dimensions.get("window").width;

leftHeader = navigation => {
  return (
    <Ionicons
      name="md-menu"
      color="#000000"
      size={32}
      style={styles.menuIcon}
      onPress={() => navigation.toggleDrawer()}
    />
  );
};

const stackNavigator = createStackNavigator(
  {
    Landing: { screen: Landing },
  },
  {
    defaultNavigationOptions: ({ navigation }) => {
      return {
        headerLeft: this.leftHeader(navigation),

        headerStyle: {
          height:55,
          backgroundColor: "#f4511e"
        },
        headerTintColor: "#fff",
        headerTitleStyle: {
          fontWeight: "bold"
        }
      };
    }
  }
);

const styles = StyleSheet.create({
  menuIcon: {
    marginLeft: 20
  },

  shoppingCartIcon: {
    marginRight: 30
  }
});
const AppContainer = createAppContainer(stackNavigator);

export default class StackNavigator extends Component{
  render(){
    return <AppContainer screenProps ={{ openDraw:()=> this.props.navigation.dispatch(DrawerActions.openDrawer())}} />
  }
}
