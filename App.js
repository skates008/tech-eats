import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";
import Header from "./views/Header";
import {createStore} from "redux";
import {Provider} from "react-redux";

import DrawerNavigator from "./navigation/DrawerNavigator";

const InitialState = {
  clientid :0,
}

const reducer = (state = InitialState, action) => { 
  switch (action.type)
  {
    case 'SET_LOGINDATA':
    return {clientid : action.value};
  }

return state; 
 
  
}

const sdstore = createStore(reducer);

class App extends Component {
  render() {
    return(
    <Provider store = {sdstore} >  
      <DrawerNavigator/>
      </Provider> 
      );
    
  }
}
export default App;







const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    paddingTop: 10,
    backgroundColor: "#ecf0f1",
    padding: 8
  },
  paragraph: {
    margin: 24,
    fontSize: 18,
    fontWeight: "bold",
    textAlign: "center"
  },
  image: {
    flex: 1,
    height: 300
  }
});
