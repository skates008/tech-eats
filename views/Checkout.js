import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Button,
  TextInput,
  Header,
  Left,
  Right,
  TouchableOpacity
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import MenuButton from "../components/MenuButton";

export default class Checkout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      specialInstruction: ""
    };
  }
  renderHeader() {
    return (
      <View style={styles.tr}>
        <Text style={styles.th}>Product</Text>

        <Text style={styles.th}>Quantity</Text>

        <Text style={styles.th}>Final Sum</Text>
      </View>
    );
  }

  renderData() {
    return (
      <View>
        <View style={styles.tr}>
          <Text style={styles.td}>Product 123</Text>

          <Text style={styles.td}>Product 12311111111111111111</Text>

          <Text style={styles.td}>Product 123</Text>
        </View>

        <View style={styles.tr}>
          <Text style={styles.td}></Text>

          <Text style={styles.td}>Final Sum</Text>

          <Text style={styles.td}>20 QAR</Text>
        </View>
      </View>
    );
  }

  render() {
    const data = [1, 2, 3, 4, 5];
    return (
      <View>
        <View style={styles.panel}>
          <Text style={styles.panelText}>review cart</Text>
        </View>
        <View style={styles.table}>
          {this.renderHeader()}
          <View>{this.renderData()}</View>
        </View>

        <View>
          <Text>
            ** Enter any special instruction for kitchen, Egg:More spicy
          </Text>
          <View style={styles.textAreaContainer}>
            <TextInput
              style={styles.textArea}
              underlineColorAndroid="transparent"
              placeholder="Type something"
              placeholderTextColor="grey"
              numberOfLines={10}
              multiline={true}
              onChangeText={specialInstruction =>
                this.setState({ specialInstruction })
              }
              value={this.state.specialInstruction}
            />
          </View>

          <View style={styles.cartTotal}>
            <View style={styles.rightLeftAlignment}>
              <Text>Sub Total</Text>
              <Text>QAR</Text>
            </View>

            <View style={styles.rightLeftAlignment}>
              <Text>Delivery Charges</Text>
              <Text>QAR</Text>
            </View>

            <View style={styles.rightLeftAlignment}>
              <Text>Discount Applied (%) </Text>
              <Text>QAR</Text>
            </View>

            <View style={styles.cartTotalPrice }>
              <Text>Total </Text>
              <Text>72 QAR</Text>
            </View>
          </View>

          <View style={{marginTop:10}}>
          <TouchableOpacity style={styles.btnNext}>
                <Text style={styles.textNextBtn}>Next</Text>
              </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  panel: {
    backgroundColor: "#005697",
    color: "#fff",
    textTransform: "uppercase",
    display: "flex",
    alignItems: "stretch"
  },
  panelText: {
    fontSize: 16,
    padding: 20,
    color: "#fff",
    textTransform: "uppercase"
  },
  accorHead: {
    backgroundColor: "#fff",
    color: "#005697",
    marginRight: 10,
    borderRadius: 50,
    width: 30,
    height: 30,
    textAlign: "center",
    lineHeight: 29
  },
  table: { width: "100%" },
  tr: {
    display: "flex",
    flexDirection: "row",
    flexWrap: "wrap",
    margin: 8
    //borderWidth: 1 //px solid rgba(3,3,3,0.2)',
  },
  td: {
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 120
    // borderWidth: 0.5
  },
  th: {
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 100
    //borderWidth: 0.5 //0.5px solid rgba(3,3,3,0.2);
  },
  textAreaContainer: {
    borderColor: "grey",
    borderWidth: 1,
    padding: 5,
    marginBottom:10,
    borderRadius:4,
  },
  textArea: {
    height: 150,
    justifyContent: "flex-start",
  },
  cartTotal: {
    backgroundColor: "#E6E6E6",
    padding: 10
  },
  rightLeftAlignment: {
    flexDirection: "row",
    justifyContent: "space-between"
  },
  cartTotalPrice:{
    flexDirection: "row",
    justifyContent: "space-between",
    borderTopWidth: 2,
    borderTopColor:'#fff',
    marginTop: 10,
    paddingTop: 10,
  },
  btnNext:{
    width: '100%',
    backgroundColor: '#005697',
    borderRadius: 50,
    paddingTop: 8,
    paddingRight: 0,
    paddingBottom: 8,
    paddingLeft: 0,
  },
  textNextBtn: {
    textAlign: "center",
    color: "#fff"
  },
});
