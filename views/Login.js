import React, { Component } from "react";
import {
  Text,
  TouchableOpacity,
  StyleSheet,
  View,
  Button,
  TextInput
} from "react-native";
import Modal from "react-native-modal";
import { Entypo } from "@expo/vector-icons";
import { config } from "../config/config";
import { connect} from "react-redux";



class Login extends Component 
{
  constructor(props) {
    super(props);
    this.state = {
      phonenumber: "",
      showErrorMessage: false,
      showInvalidUserMessage: false,
      clientidlocal : 0

    };
  }

  static navigationOptions = {
    title: "Login",
    headerStyle: {
      backgroundColor: "#f4511e"
    }
  };

  state = {
    visibleModal: null
  };

  login = () => {
    if (this.state.phonenumber.length != 8)
      this.setState(state => ({ showErrorMessage: true }));
    else {
      this.setState(state => ({ showErrorMessage: false }));
      const phone = this.state.phonenumber;
      let formdata = new FormData();

      formdata.append("phone", phone);
      fetch(config.BASE_URL + "user/login", {
        method: "POST",
        headers: {
          "Content-Type": "multipart/form-data"
        },
        body: formdata
      })
        .then(response => response.json())
        .then(response => {
          console.log(response);
          if (!response.status) {
            this.setState(state => ({ showInvalidUserMessage: true }));
          } else {
            this.setState(state => ({ showInvalidUserMessage: false }));




            alert(response.message.userID);

            this.props.setlogindata(response.message.userID);


            // this.state.clientidlocal = response.message.userID


            
          }
        })
        .catch(error => {
          console.log(error);
        })
        .done();
    }
  };

  renderErrorMessage() {
    const self = this.state;
    if (self.showErrorMessage) {
      return (
        <View style={styles.errorMsg}>
          <Text>Enter Valid Phone Number</Text>
        </View>
      );
    }

    if (self.showInvalidUserMessage) {
      return (
        <View style={styles.errorMsg}>
          <Text>Invalid User</Text>
        </View>
      );
    }
  }



  render() {
    return (
      <View style={styles.container}>
        <Text style={{ fontSize: 30 }}> Login </Text>
        <View style={styles.inputField}>
          <Text>+974</Text>
          <TextInput
            keyboardType="numeric"
            style={styles.inputPhoneNumber}
            placeholder="Enter Phone No."
            onChangeText={phonenumber => this.setState({ phonenumber })}
          />
        </View>

        <View>
          <TouchableOpacity onPress={() => this.login()}>
            <Text style={styles.btnContinue}>Continue</Text>
            <Text>{this.props.clientid}</Text>
          </TouchableOpacity>
        </View>
        {this.renderErrorMessage()}
      </View>
    );
  }
}



function mapStateToProps(state){
   return{
  clientid : state.clientid
   }
}

function mapDispatchToProps(dispatch){
   return{
      setlogindata : (sde) => dispatch({type:'SET_LOGINDATA', value: sde}),
      // setlogindata : () => dispatch({type:'SET_LOGINDATA',value:this.state.clientidlocal }),
   }
}

export default  connect (mapStateToProps , mapDispatchToProps)(Login) 



const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  btnContinue: {
    backgroundColor: "#015697",
    color: "#fff",
    width: 100,
    textAlign: "center",
    marginTop: 20,
    paddingTop: 6,
    paddingBottom: 6,
    borderRadius: 50
  },
  inputField: {
    width: "80%",
    borderColor: "#ccc",
    borderWidth: 1,
    padding: 5,
    borderRadius: 4
  },
  inputPhoneNumber: {
    width: "100%",
    borderLeftWidth: 1,
    borderLeftColor: "#ccc",
    paddingLeft: 11
  },
  errorMsg: {
    marginTop: 10,
    color: "#31708f",
    backgroundColor: "#d9edf7",
    borderColor: "#bce8f1",
    padding: 15,
    marginBottom: 20,
    borderColor: "transparent",
    borderWidth: 1,
    borderRadius: 4
  }
});
