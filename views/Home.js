import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Button,
  Container,
  Header,
  Left,
  Right
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import MenuButton from "../components/MenuButton";

export default class Home extends Component {
 

  render() {
    return (
      <View style={styles.container}>
      
      <Text >Home</Text>
    
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});
