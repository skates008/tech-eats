import React, { Component, createRef } from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  ImageBackground,
  Dimensions,
  TouchableOpacity,
  TextInput,
  ActivityIndicator,
  Alert
} from "react-native";
import { Icon, Button } from "react-native-elements";
import withBadge from "../components/withBadge";
import { Platform } from "react-native";

import { config } from "../config/config";
import { FlatList } from "react-native-gesture-handler";
// import ProductItemList from "../components/ProductItemList";

const width = Dimensions.get("window").width;
export default class Order extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    const itemsCount = params.countItems == undefined ? 0 : params.countItems;
    const BadgedIcon = withBadge(itemsCount)(Icon);
    return {
      headerStyle: {
        backgroundColor: "#f4511e"
      },
      headerRight: (
        <BadgedIcon
          name={`${Platform.OS === "ios" ? "ios" : "md"}-cart`}
          type="ionicon"
          size={30}
          color="white"
          containerStyle={styles.shoppingCartIcon}
          onPress={() => params.handleThis()}
        />
      )
    };
  };
  constructor(props) {
    super(props);
    this.addToCart = this.addToCart.bind(this);
    this.arr = [];
    this.addedItems = [];
    this.state = {
      menusCollection: [],
      subCategoryCollection: [],
      selectedTime: "",
      selectedDate: "",
      refs: "",
      dynamicIndex: 0,
      contentLoading: 1,
      vModel: {
        city: "",
        location: "",
        time: "",
        date: "",
        deliveryCharge: ""
      },
      addedItems: [],
      isCartDetailsVisible: false,
      selectedItems: [],
      discountItems: [],
      subTotal: "",
      couponCode: "",
      discountAmount: "",
      discountApplied: 0,
      disableApplyBtn: false
    };
  }

  showCartDetails = () => {
    this.setState(prevState => ({
      isCartDetailsVisible: !prevState.isCartDetailsVisible
    }));
  };

  componentWillMount() {
    console.log("wmount");
  }

  componentWillReceiveProps(newProps) {
    console.log("componentWillReceiveProps");
    this.setState({
      vModel: {
        city: newProps.navigation.getParam("city", ""),
        location: newProps.navigation.getParam("location", ""),
        date: newProps.navigation.getParam("date", ""),
        time: newProps.navigation.getParam("time", ""),
        deliveryCharge: newProps.navigation.getParam("deliveryCharge", 0)
      }
    });
  }

  componentDidUpdate() {
    //console.log("componentDidUpdate");
  }

  componentWillUpdate() {
    //console.log("componentWillUpdate");
  }

  componentDidCatch() {
    console.log("componentDidCatch");
  }

  componentDidMount() {
    const { navigation } = this.props;
    this.props.navigation.setParams({
      handleThis: this.showCartDetails
    });

    fetch(config.BASE_URL + "/menus")
      .then(response => response.json())
      .then(responseJson => {
        this.setState({
          menusCollection: responseJson,
          contentLoading: 0,
          vModel: {
            city: navigation.getParam("city", ""),
            location: navigation.getParam("location", ""),
            date: navigation.getParam("date", ""),
            time: navigation.getParam("time", ""),
            deliveryCharge: navigation.getParam("deliveryCharge", 0)
          }
        });
      })
      .catch(error => {
        console.error(error);
      });
    this.getDiscountCodes();
  }

  getDiscountCodes() {
    fetch(config.BASE_URL + "/discountcode", {
      method: "POST"
    })
      .then(response => response.json())
      .then(responseJson => {
        this.setState({
          discountItems: responseJson
        });
      })
      .catch(error => {
        console.error(error);
      });
  }

  handleClick(id) {
    this.scrollview_ref.scrollToIndex({ animated: true, index: id });
  }

  menuTitles() {
    const menusCollection = this.state.menusCollection;
    return menusCollection.map((menu, index) => {
      return (
        <View key={index} style={styles.menuBox}>
          <Text
            style={styles.menuBoxTitle}
            onPress={this.handleClick.bind(this, index)}
          >
            {menu.name}
          </Text>
        </View>
      );
    });
  }

  subCategory() {
    var _this = this;
    const menusCollection = this.state.menusCollection;
    return (
      <View>
        <FlatList
          data={menusCollection}
          ref={ref => {
            this.scrollview_ref = ref;
          }}
          renderItem={({ item, index, separators }) => (
            <View
              key={index}
              ref={item.id}
              style={{ marginTop: 10 }}
              onLayout={event => {
                const layout = event.nativeEvent.layout;
                this.arr[index] = layout.y;
              }}
            >
              <Text style={styles.menuListHead}>{item.name}</Text>
              {item.subcategory.map(function(sc, indexsc) {
                return (
                  <View key={indexsc}>
                    <Text style={styles.subCategoryName}>{sc.name}</Text>

                    <View>
                      <View style={styles.itemList}>
                        {sc.menus.map((subMenu, indexsc1) => {
                          return (
                            <View
                              key={indexsc1}
                              style={[styles.box, styles.shadow3]}
                            >
                              <ImageBackground
                                source={{
                                  uri: subMenu.image
                                }}
                                style={styles.imageCard}
                              ></ImageBackground>
                              <Text>{subMenu.title}</Text>
                              <Text style={styles.itemPrice}>
                                {subMenu.price} QAR
                              </Text>

                              <View style={styles.addToCartView}>
                                <TouchableOpacity
                                  onPress={() => _this.addToCart(subMenu)}
                                >
                                  <Text style={styles.btnAddCart}>Add</Text>
                                </TouchableOpacity>
                              </View>
                            </View>
                          );
                        })}
                      </View>
                    </View>
                  </View>
                );
              })}
            </View>
          )}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  }

  addToCart = incomingItem => {
    const addedItems = this.state.addedItems;
    if (incomingItem && incomingItem.id) {
      incomingItem.count =
        incomingItem.count == undefined ? 1 : incomingItem.count;
      if (addedItems.length > 0) {
        var index = addedItems.findIndex(item => item.id === incomingItem.id);
        if (index != -1) {
          var stateCopy = Object.assign({}, this.state);
          stateCopy.addedItems[index].count += 1;
          this.setState(stateCopy);
        } else {
          this.setState({
            addedItems: [...this.state.addedItems, incomingItem]
          });
        }
      } else {
        this.setState({ addedItems: [...this.state.addedItems, incomingItem] });
      }
      this.alertMessage(incomingItem);
      this.countItems();
      this.subTotalPrice();
    }
  };

  alertMessage(incomingItem) {
    Alert.alert(
      "Tech-Eats",

      `${incomingItem.title} has been added to Cart!!`,
      [{ text: "OK", onPress: () => console.log("OK Pressed") }],
      { cancelable: true }
    );
  }

  countItems() {
    setTimeout(() => {
      this.props.navigation.setParams({
        countItems: this.state.addedItems.length
      });
    }, 100);
  }

  changePreferences() {
    this.props.navigation.navigate("Landing");
  }

  renderDeliveryInformation() {
    const vModel = this.state.vModel;
    if (vModel.city && vModel.location) {
      return (
        <View style={styles.deliveryInformation}>
          <View style={{ flexDirection: "row" }}>
            <View style={{ flex: 1 }}>
              <Text>Delivery Location</Text>
              <Text style={{ justifyContent: "flex-start", fontWeight: "500" }}>
                {vModel.city}, {vModel.location}
              </Text>
            </View>
            <View style={styles.deliveryTime}>
              <Text>Delivery Time</Text>
              <Text style={{ fontWeight: "500" }}>
                {vModel.time == "" ? "Approx 1 hour" : vModel.time}
              </Text>
              <Text>{vModel.date == "" ? "" : vModel.date}</Text>
            </View>
          </View>

          <View style={styles.viewBtnPreferences}>
            <TouchableOpacity onPress={() => this.changePreferences()}>
              <Text style={styles.btnPreferences}>Change Preferences</Text>
            </TouchableOpacity>
          </View>
        </View>
      );
    }
  }

  decrement = (count, id, index) => {
    count = count - 1;
    var data = [...this.state.addedItems];
    if (count <= 0) {
      let filteredArray = this.state.addedItems.filter(item => item.id !== id);
      this.setState({ addedItems: filteredArray });
      this.subTotalPrice();
      this.getDiscountApplied();
      this.countItems();
      return;
    }
    var index = data.findIndex(obj => obj.id === id);

    data[index].count = count;
    this.subTotalPrice();
    this.getDiscountApplied();
    this.setState({ data });
  };

  increment = (id, index) => {
    var data = [...this.state.addedItems];

    var index = data.findIndex(obj => obj.id === id);

    data[index].count++;
    this.subTotalPrice();
    this.getDiscountApplied();
    this.setState({ data });
  };

  renderCartItems() {
    var _this = this;
    const state = _this.state;
    if (state.addedItems && state.addedItems.length > 0) {
      return (
        <View style={styles.cartContainer}>
          <View style={{ height: "30%" }}>
            <FlatList
              data={state.addedItems}
              renderItem={({ item, index, separators }) => (
                <View>
                  <View style={styles.rightLeftAlignment}>
                    <Text style={styles.totalPrice}>{item.title}</Text>

                    <View style={styles.numericInput}>
                      <Button
                        buttonStyle={styles.leftButtonStyle}
                        icon={{
                          name: "remove",
                          color: "white",
                          size: 10
                        }}
                        onPress={() =>
                          _this.decrement(item.count, item.id, index)
                        }
                      ></Button>
                      <Text> {item.count} </Text>
                      <Button
                        buttonStyle={styles.rightButtonStyle}
                        icon={{ name: "add", color: "white", size: 10 }}
                        onPress={() => _this.increment(item.id, index)}
                      ></Button>
                    </View>
                  </View>
                </View>
              )}
              keyExtractor={(item, index) => index.toString()}
            />
          </View>
          <View style={styles.cartTotalPrice}>
            <View style={styles.rightLeftAlignment}>
              <Text style={styles.subTotalText}>Sub Total</Text>
              <Text style={styles.subTotalPrice}> {state.subTotal} QAR</Text>
            </View>

            <View style={styles.rightLeftAlignment}>
              <Text style={styles.subTotalText}>Delivery Charge</Text>
              <Text style={styles.subTotalPrice}>
                {state.vModel.deliveryCharge} QAR
              </Text>
            </View>

            <View style={styles.rightLeftAlignment}>
              <Text style={styles.subTotalText}>Discount Applied</Text>
              <Text style={styles.subTotalPrice}>{state.discountApplied}</Text>
            </View>

            <View>
              {state.discountItems.map((item, index) => {
                return (
                  <Text key={index} style={styles.coupon}>
                    Use coupon code {item.code} to get {item.amount}
                    {item.type == "percent" ? "%" : ""} discount.
                  </Text>
                );
              })}
            </View>

            <View style={styles.couponForm}>
              <TextInput
                editable={!state.disableApplyBtn}
                style={styles.textInputCoupon}
                value={this.state.couponCode}
                onChangeText={couponCode => this.setState({ couponCode })}
                placeholder="Coupon Code"
              />
            </View>

            <View style={{ paddingBottom: 20 }}>
              <TouchableOpacity
                disabled={state.disableApplyBtn}
                activeOpacity={state.disableApplyBtn ? 1 : 0.5}
                style={[
                  styles.discountApplyButton,
                  {
                    backgroundColor: state.disableApplyBtn
                      ? "#607D8B"
                      : "#005697"
                  }
                ]}
                onPress={() => this.applyButton()}
              >
                <Text style={styles.textApplyBtn}>Apply</Text>
              </TouchableOpacity>
            </View>

            <View style={styles.cartTotalCheckout}>
              <View style={styles.cartTotalView}>
                <View style={styles.rightLeftAlignment}>
                  <Text style={styles.totalPrice}>Total</Text>
                  <Text style={styles.totalPrice}>
                    {_this.totalPrice("")} QAR
                  </Text>
                </View>
              </View>

              <View>
                <TouchableOpacity
                  style={styles.discountApplyButton}
                  onPress={() => this.checkoutButton( )}
                >
                  <Text style={styles.textCheckoutBtn}>
                    PROCEED TO CHECKOUT
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      );
    } else {
      return (
        <View style={styles.cartContainer}>
          <Text>No items added yet</Text>
        </View>
      );
    }
  }

  subTotalPrice() {
    setTimeout(() => {
      const addedItems = this.state.addedItems;
      const subTotal = addedItems.reduce(
        (sum, element) =>
          sum + parseFloat(element.price) * parseFloat(element.count),
        0
      );

      this.setState({ subTotal: subTotal });
    }, 100);
  }

  getDiscountApplied() {
    setTimeout(() => {
      const _this = this;
      const subTotal = _this.state.subTotal;
      const discountAmount = _this.discountAmount;

      var discountApplied =
        discountAmount == undefined
          ? 0
          : ((discountAmount / 100) * subTotal).toFixed(2);

      this.setState({ discountApplied: discountApplied });
    }, 100);
  }

  totalPrice() {
    const state = this.state;
    return (
      parseFloat(state.subTotal) +
      parseFloat(state.vModel.deliveryCharge) -
      parseFloat(state.discountApplied)
    );
  }

  applyButton() {
    const couponCode = this.state.couponCode;
    const discountItems = this.state.discountItems;
    if (couponCode) {
      var dicscountObj = discountItems.find(x => x.code == couponCode);

      if (dicscountObj != null) {
        this.discountAmount = dicscountObj.amount;
        this.getDiscountApplied();
        this.setState({
          discountAmount: dicscountObj.amount,
          disableApplyBtn: true
        });
      }
    }
  }

  renderCartDetails() {
    const isCartDetailsVisible = this.state.isCartDetailsVisible;
    const addedItems = this.state.addedItems;

    const countCart = addedItems == undefined ? 0 : addedItems.length;
    if (isCartDetailsVisible) {
      return (
        <View style={{ height: "100%", paddingRight: 5 }}>
          <View style={styles.cartStyle}>
            <View style={styles.cartBoxHeader}>
              <Text style={styles.cartItem}>Your Cart: {countCart} Items</Text>
              <Text style={styles.cartPrice}>QAR</Text>
            </View>

            {this.renderCartItems()}
          </View>
        </View>
      );
    }
  }

  checkoutButton(totalpricing) {
    console.log("1");

    const state = this.state;

    let formdata = new FormData();

    formdata.append("city", state.vModel.location);
    formdata.append("total", state.subTotal);

    fetch(config.BASE_URL + "checkout/minorder", {
      method: "POST",
      headers: {
        "Content-Type": "multipart/form-data"
      },
      body: formdata
    })
      .then(response => response.json())
      .then(response => {
        console.log(response);
        if (!response.status) {
          // this.setState(state => ({ showInvalidUserMessage: true }));
          var neviokey = 0;
          alert(response.message);
        } else {
          this.props.navigation.navigate("Checkout", {});
        }
      })
      .catch(error => {
        console.log(error);
      })
      .done();
  }

  render() {
    if (this.state.contentLoading) {
      return (
        <ActivityIndicator
          size="large"
          style={{ flex: 1, justifyContent: "center" }}
        />
      );
    }
    return (
      <View style={styles.container}>
        {this.renderCartDetails()}
        {this.renderDeliveryInformation()}

        <View style={{ height: 80, paddingRight: 5 }}>
          <ScrollView horizontal showsHorizontalScrollIndicator={false}>
            {this.menuTitles()}
          </ScrollView>
        </View>
        {this.subCategory()}
      </View>
    );
  }
}

function elevationShadowStyle(elevation) {
  return {
    elevation,
    shadowColor: "black",
    shadowOffset: { width: 0, height: 0.5 * elevation },
    shadowOpacity: 0.3,
    shadowRadius: 0.8 * elevation
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 10,
    backgroundColor: "#fff"
  },
  shadow3: elevationShadowStyle(20),

  box: {
    position: "relative",
    marginBottom: 20,
    borderRadius: 8,
    backgroundColor: "white",
    padding: 24,
    width: width / 2 - 15,
    height: "auto",
    marginLeft: 10,
    marginTop: 10
  },

  addToCartView: {
    position: "absolute",
    bottom: "10%",
    right: 10,
    justifyContent: "flex-end",
    flex: 1,
    flexDirection: "row"
  },
  menuListHead: {
    width: "100%",
    margin: 0,
    backgroundColor: "#005697",
    color: "#fff",
    fontSize: 20,
    paddingTop: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingLeft: 10,
    textTransform: "uppercase"
  },
  subCategoryName: {
    textTransform: "capitalize",
    fontSize: 30
  },
  itemList: {
    flexDirection: "row",
    flexWrap: "wrap"
  },
  imageCard: {
    borderRadius: 128,
    width: "auto",
    height: 100
  },
  itemPrice: {
    color: "#808080",
    marginTop: 4
  },
  btnAddCart: {
    color: "#fff",
    backgroundColor: "#005697",
    paddingTop: 3,
    paddingRight: 14,
    paddingBottom: 3,
    paddingLeft: 14,
    borderRadius: 50
  },
  menuBox: {
    width: "auto",
    marginTop: 5,
    padding: 5
  },

  menuBoxTitle: {
    //flexGrow:,
    width: "100%",
    backgroundColor: "#005697",
    borderColor: "#005697",
    borderWidth: 1,
    display: "flex",
    borderRadius: 5,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
    paddingTop: 15,
    paddingRight: 10,
    paddingBottom: 15,
    paddingLeft: 10,
    color: "#fff",
    fontSize: 13,
    height: 50,
    fontWeight: "bold"
  },

  menuTitle: {
    margin: 0,
    backgroundColor: "#005697",
    color: "#fff",
    fontSize: 20,
    padding: 10,
    textTransform: "uppercase"
  },

  itemName: {
    color: "#005697",
    fontSize: 14,
    fontWeight: "500"
  },

  deliveryInformation: {
    borderWidth: 1,
    borderColor: "#808080",
    borderRadius: 4,
    padding: 15,
    margin: 5
  },

  deliveryTime: {
    //textAlign: "right"
    justifyContent: "flex-end"
  },
  fontWeight500: {
    fontWeight: "500"
  },
  viewBtnPreferences: {
    width: "100%",
    flexDirection: "column"
  },
  btnPreferences: {
    width: "100%",
    color: "#005697",
    borderWidth: 1,
    borderColor: "#005697",
    textAlign: "center",
    padding: 5,
    borderRadius: 50,
    marginTop: 5
  },
  shoppingCartIcon: {
    marginRight: 30
  },
  viewCartPrice: {},
  cartStyle: {
    display: "flex",
    backgroundColor: "#fff"
  },
  cartBoxHeader: {
    flexDirection: "row",
    justifyContent: "space-between",
    backgroundColor: "#005697",
    color: "#fff",
    paddingTop: 15,
    paddingRight: 10,
    paddingBottom: 15,
    paddingLeft: 10,
    borderRadius: 1
  },
  cartItem: {
    paddingTop: 5,
    paddingBottom: 5,
    color: "#fff"
  },
  cartPrice: {
    color: "#fff",
    paddingLeft: 20,
    borderLeftWidth: 1,
    borderLeftColor: "#fff"
  },
  cartContainer: {
    borderWidth: 1,
    borderColor: "#005697",
    padding: 10
  },
  cartTotalPrice: {
    marginTop: 20,
    paddingTop: 10,
    borderTopWidth: 2,
    borderColor: "#ccc",

    //borderColor: '#005697',
    padding: 10
  },
  subTotalText: {},
  subTotalPrice: {},

  rightLeftAlignment: {
    flexDirection: "row",
    justifyContent: "space-between"
  },

  coupon: {
    textAlign: "center",
    paddingTop: 0,
    paddingRight: 4,
    paddingBottom: 7,
    paddingLeft: 4,
    color: "#b3af10",
    fontSize: 14,
    fontWeight: "900"
  },

  textInputCoupon: {
    marginBottom: 10,
    borderRadius: 50,
    borderWidth: 1,
    borderColor: "#333",
    paddingTop: 5,
    paddingRight: 20,
    paddingBottom: 5,
    paddingLeft: 20
  },
  discountApplyButton: {
    borderRadius: 50,
    backgroundColor: "#005697",
    color: "#fff",
    paddingTop: 6,
    paddingBottom: 6
  },
  textApplyBtn: {
    textAlign: "center",
    color: "#fff"
  },
  cartTotalCheckout: {
    paddingTop: 10,
    paddingBottom: 10
  },
  cartTotalView: {
    borderTopWidth: 2,
    borderTopColor: "#ccc",
    paddingTop: 10,
    paddingBottom: 10
  },
  textCheckoutBtn: {
    textAlign: "center",
    color: "#fff",
    textTransform: "uppercase",
    fontSize: 16,
    fontWeight: "500"
  },
  totalText: {},
  totalPrice: {
    color: "#808080",
    fontSize: 16
  },

  rightButtonStyle: {
    height: 28,
    justifyContent: "center",
    alignItems: "center",
    borderWidth: 0,
    backgroundColor: "#337ab7",
    borderRadius: 5,
    width: 35,
    borderWidth: 1,
    borderColor: "#2e6153"
  },
  leftButtonStyle: {
    backgroundColor: "#d9534f",
    height: 28,
    borderRadius: 5,
    width: 35,
    borderWidth: 1,
    borderColor: "#2e6153"
  },
  numericInput: {
    justifyContent: "flex-end",
    flexDirection: "row",
    flexWrap: "wrap",
    flex: 1,
    marginBottom: 5
  }
});
