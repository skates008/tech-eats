import React, {Component} from "react";
import {View, ActivityIndicator,TouchableOpacity,Text, StyleSheet, ScrollView} from "react-native";
import { config } from "../config/config";
import Icon from "react-native-vector-icons/MaterialIcons";
import { connect} from "react-redux";



class Accordian extends Component{

    constructor(props) {
        super(props);
        this.state = { 
        //   data: props.data,
          expanded : false,
          isLoading: true,
          myOrders: null
        }
    }
    static navigationOptions = {
        title: "My Orders",
        headerStyle: {
          backgroundColor: "#f4511e"
        },
        
      };
    componentDidMount() {
        const formData = new FormData();
        
        formData.append('client_id',this.props.clientid);
        fetch(config.BASE_URL+ "/myorders", {
            method: 'post',
            headers: {'Content-Type': 'multipart/form-data'},
            body:formData
            })
            .then(response => response.json())
            .then(responseJson => {
                responseJson.map((item, index) => {
                    item['expanded'] = false
                });
                this.setState({
                    myOrders: responseJson,
                    isLoading: false
                })
            })
            .catch(error=>{
                console.error(error);
            })
    }
    
  render() {
    if (this.state.isLoading){
        return (
            <ActivityIndicator size="large" style={{flex:1, justifyContent: 'center'}} />
        )
    }
    const orderItems = this.state.myOrders;
    // console.log(orderItems)
    return (        
       <ScrollView style={styles.scrollView}>
           {
               orderItems.map((item, index) => {
                
                return (
                    <View key={index} style={styles.container1}>
                        <TouchableOpacity style={styles.row} onPress={this.toggleExpand.bind(this, index)}>
                            <Text style={[styles.title, styles.font]}>
                                {item.date}{'\n'}
                                Order num: #{item.order_id}{'\n'}
                                QAR {item.grandtotal}
                            </Text>
                            {/* <Icon name={orderItems[index]['expanded'] ? 'keyboard-arrow-up' : 'keyboard-arrow-down'} size={30} color='white' /> */}
                            <Text style={styles.btnOrder}>Details</Text>
                            
                        </TouchableOpacity>
                        <View style={styles.orderItems}>
                        {                            
                            orderItems[index]['expanded'] &&
                            <View style={styles.child}>
                                <View>
                                    <Text>Order Item List</Text>
                                    <View style={{borderBottomColor: 'black',borderBottomWidth: 1,paddingBottom:5}} />
                                    {item.products.map((product, i)=>{
                                        return(
                                            <View key={i}>
                                                <Text style={styles.productDetails}>
                                                    {product.title}     {product.quantity}*{product.price}     
                                                </Text>                                       
                                            </View>
                                            )
                                  })}
                                </View>
                                <View style={{borderBottomColor: 'black',borderBottomWidth: 1}}></View>
                                <View stype={styles.subtotal}>
                                    <Text style={color='white', textAlign='right'}>{'\n'}Subtotal Amount:</Text><Text>QAR {item.subtotal}{'\n'}</Text>
                                </View>
                                <View style={{borderBottomColor: 'black',borderBottomWidth: 1}}></View>
                                <View stype={styles.deliveryCharges}>
                                    <Text style={color='white', textAlign='right'}>{'\n'}Delivery Amount:</Text><Text>QAR {item.delivery_charges}{'\n'}</Text>
                                </View>
                                <View style={{borderBottomColor: 'black',borderBottomWidth: 1}}></View>
                                <Text style={color='white', textAlign='right'}>Total Amount:</Text><Text>QAR {item.grandtotal}</Text>
                                <View style={{borderBottomColor: 'black',borderBottomWidth: 1}}></View>
                                <Text style={styles.orderInfo}>{'\n'}Payment Type: {item.payment_type}{'\n'}
                                    Address: {item.address}
                                </Text>
                                
                                
                            </View>
                        }
                        </View>
                    </View>
                )
               })
           }
       </ScrollView>
    )
  }

  toggleExpand=(index)=>{
   myOrdersCopy = this.state.myOrders;
   myOrdersCopy[index]['expanded'] = !myOrdersCopy[index]['expanded']
   this.setState({myOrders: myOrdersCopy})
  }

}

function mapStateToProps(state){
   return{
  clientid : state.clientid
   }
}



export default  connect(mapStateToProps)(Accordian)

const styles = StyleSheet.create({
    container1:{
        flex: 1,
        backgroundColor: "#fff"
    },
    title:{
        fontSize: 12,
        fontWeight:'bold',
        color: 'white',
    },
    row:{
        flexDirection: 'row',
        justifyContent:'space-between',
        height:70,
        paddingLeft:20,
        paddingRight:13,
        alignItems:'center',
        backgroundColor: '#005697',
        margin:5
    },
    parentHr:{
        height:1,
        color: 'white',
        width:'100%'
    },
    child:{
        backgroundColor: 'lightgray',
        padding:16
    },
    scrollView: {
        marginHorizontal: 20,
    },
    productDetails:{
        fontSize:10
    },
    orderInfo:{
        color:'white',
        padding:3,
        fontSize:12
    },
    subtotal:{
        flexDirection: 'row',
        justifyContent:'space-between',
    },
    deliveryCharges:{
        flexDirection: 'row',
        justifyContent:'space-between',
        alignItems:'center'
    }
    
});