import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Button,
  Picker,
  ImageBackground,
  TouchableOpacity,
  Platform
} from "react-native";
import SearchableDropdown from "react-native-searchable-dropdown";
import MenuButton from "../components/MenuButton";
import moment from "moment";
import { Ionicons, FontAwesome } from "@expo/vector-icons";
import { config } from "../config/config";
var backgroundImage = require("../assets/home_back_img.jpg");
export default class Home extends Component {
  static navigationOptions = {
    title: "Landing",
    headerStyle: {
      backgroundColor: "#f4511e"
    }
  };

  constructor() {
    super();
    this.state = {
      citiesCollection: [],
      locationCollection: [],
      dateList: [],
      timeList: [],

      vModel: {
        city: "",
        location: "",
        selectedTime: "",
        selectedDate: "",
        deliveryCharge: ""
      },
      showdateTime: false,
      isProcessing: false,
      showErrorMessage: false,
      showDateTimeErrorMessage: false,
      showPlaceOrderView: false
    };
  }

  onCityChangeEvent = items => {
    this.state.vModel.city = items.name;
    this.state.vModel.deliveryCharge = items.deliverycharge;
    this.state.locationCollection = [];
    const cities = this.state.citiesCollection;
    if (cities && cities.length > 0 && items.name) {
      this.setState({
        locationCollection: [...this.state.locationCollection, ...items.child]
      });
    }
  };

  checkCityLocation = (city, location) => {
    if (city && location) {
      this.setState(state => ({ showErrorMessage: false }));
      return true;
    } else {
      this.setState(state => ({ showErrorMessage: true }));
      return false;
    }
  };

  checkDateTime = (date, time) => {
    if (date && time) {
      this.setState(state => ({ showDateTimeErrorMessage: false }));
      return true;
    } else {
      this.setState(state => ({ showDateTimeErrorMessage: true }));
      return false;
    }
  };

  orderNow = () => {
    const model = this.state.vModel;
    this.setState(state => ({ isProcessing: true }));
    var isValid = this.checkCityLocation(model.city, model.location);
    if (isValid) {
      this.props.navigation.navigate("Order", {
        city: model.city,
        location: model.location,
        deliveryCharge: model.deliveryCharge
      });
    }
  };

  placeOrder = () => {
    const model = this.state.vModel;
    this.setState(state => ({ isProcessing: true }));
    var isValid = this.checkCityLocation(model.city, model.location);
    if (isValid) {
      var isDateTimeValid = this.checkDateTime(
        model.selectedDate,
        model.selectedTime
      );
      if (isDateTimeValid)
        this.props.navigation.navigate("Order", {
          city: model.city,
          location: model.location,
          date: model.selectedDate,
          time: model.selectedTime,
          deliveryCharge: model.deliveryCharge
        });
    }
  };

  cancelOrder = () => {
    this.setState(state => ({ showPlaceOrderView: false }));
  };

  orderLaterButton = () => {
    const self = this.state;
    this.setState(state => ({ isProcessing: true }));
    if (self.vModel.city && self.vModel.location) {
      this.setState(state => ({ showPlaceOrderView: true }));

      self.showErrorMessage = false;

      this.state.showdateTime = true;
      this.state.dateList = [];
      const dateData = [];
      var todaysDate = new Date();
      for (var i = 0; i < 4; i++) {
        dateAdd = moment(todaysDate).add(i, "d");
        var finalDate = moment(dateAdd).format("D MMMM YYYY");

        dateData.push({ id: i, name: finalDate });
        this.state.dateList.push(dateData);
      }
      this.setState({ dateList: dateData });
    } else {
      self.showErrorMessage = true;
    }
  };

  onDateChangeEvent = items => {
    this.state.vModel.selectedDate = items.name;
    const timeListData = [];
    if (items.id == 0) {
      var today = new Date();
      today.setDate(today.getDate() + 1);
      var time = today.getHours();
      var i = time + 3;
      if( i < 10 )
      {
        i = 10;
      }
    } else i = 10;

    for (i; i < 23; i++) {
      if (i < 12) {
        var a = i + " am to " + (i + 1) + " am";
      }
      if (i == 12) {
        var a = "12 am to 1 pm";
      }
      if (i > 12) {
        var a = i - 12 + " pm to " + (i - 11) + " pm";
      }

      timeListData.push({ id: a, name: a });
      this.state.timeList.push(timeListData);
    }
    this.setState({ timeList: timeListData });
  };

  componentDidMount() {
    fetch(config.BASE_URL + "locations")
      .then(response => response.json())
      .then(responseJson => {
        this.setState({
          citiesCollection: [...this.state.citiesCollection, ...responseJson]
        });
      })
      .catch(error => {
        console.error(error);
      });
  }

  renderDateTimeDropdown() {
    const showDateTime = this.state.showdateTime;
    const showPlaceOrderView = this.state.showPlaceOrderView;
    if (showDateTime && showPlaceOrderView) {
      return (
        <View>
          <SearchableDropdown
            onItemSelect={item => this.onDateChangeEvent(item)}
            containerStyle={{ padding: 5 }}
            textInputStyle={{
              padding: 12,
              borderWidth: 1,
              borderColor: "#ccc",
              backgroundColor: "#FAF7F6",
              borderRadius: 5
            }}
            itemStyle={{
              padding: 10,
              marginTop: 2,
              backgroundColor: "#FAF9F8",
              borderColor: "#bbb",
              borderWidth: 1
            }}
            itemTextStyle={{
              color: "#222"
            }}
            itemsContainerStyle={{
              maxHeight: "100%"
            }}
            items={this.state.dateList}
            placeholder="Enter Date"
            resetValue={false}
            underlineColorAndroid="transparent"
          />

          <SearchableDropdown
            onItemSelect={item => (this.state.vModel.selectedTime = item.name)}
            containerStyle={{ padding: 5 }}
            textInputStyle={{
              padding: 12,
              borderWidth: 1,
              borderColor: "#ccc",
              backgroundColor: "#FAF7F6",
              borderRadius: 5
            }}
            itemStyle={{
              padding: 10,
              marginTop: 2,
              backgroundColor: "#FAF9F8",
              borderColor: "#bbb",
              borderWidth: 1
            }}
            itemTextStyle={{
              color: "#222"
            }}
            itemsContainerStyle={{
              height: "50%",
              marginBottom: 0
            }}
            items={this.state.timeList}
            defaultIndex={1}
            placeholder="Enter Time"
            resetValue={false}
            underlineColorAndroid="transparent"
          />
        </View>
      );
    }
  }
  renderErrorMessage() {
    const self = this.state;
    if (self.isProcessing && self.showErrorMessage) {
      return (
        <View style={styles.validationAlert}>
          <Text>
            <FontAwesome
              name="exclamation-triangle"
              backgroundColor="#3b5998"
            />
            Please Choose Your Location to Proceed
          </Text>
        </View>
      );
    }

    if (self.isProcessing && self.showDateTimeErrorMessage) {
      return (
        <View style={styles.validationAlert}>
          <Text>
            <FontAwesome
              name="exclamation-triangle"
              backgroundColor="#3b5998"
            />
            Please Choose Date & Time to Proceed
          </Text>
        </View>
      );
    }
  }

  renderButton() {
    const self = this.state;
    if (!self.showPlaceOrderView) {
      return (
        <View>
          <TouchableOpacity onPress={() => this.orderNow()}>
            <Text style={styles.btnOrder}>Order Now</Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => this.orderLaterButton()}>
            <Text style={styles.btnOrder}>Order For Later</Text>
          </TouchableOpacity>
        </View>
      );
    } else {
      return (
        <View>
          <TouchableOpacity onPress={() => this.placeOrder()}>
            <Text style={styles.btnOrder}>Place Order</Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => this.cancelOrder()}>
            <Text style={styles.btnCancelOrder}>Cancel Order</Text>
          </TouchableOpacity>
        </View>
      );
    }
  }

  render() {
    return (
      <View>
        <ImageBackground
          source={backgroundImage}
          style={{ width: "100%", height: "100%" }}
        >
          <View style={styles.container}>
            <View style={styles.homeTabs}>
              <SearchableDropdown
                onTextChange={text => console.log(text)}
                onItemSelect={item => this.onCityChangeEvent(item)}
                containerStyle={{ padding: 5 }}
                textInputStyle={{
                  padding: 12,
                  borderWidth: 1,
                  borderColor: "#ccc",
                  borderRadius: 5,
                  backgroundColor: "#FAF7F6"
                }}
                itemStyle={{
                  padding: 10,
                  marginTop: 2,
                  backgroundColor: "#FAF9F8",
                  borderColor: "#bbb"
                }}
                itemTextStyle={{
                  color: "#222"
                }}
                itemsContainerStyle={{
                  maxHeight: "100%"
                }}
                items={this.state.citiesCollection}
                defaultIndex={1}
                placeholder="Enter City"
                resetValue={false}
                underlineColorAndroid="transparent"
              />
              <SearchableDropdown
                onTextChange={text => console.log(text)}
                onItemSelect={item => (this.state.vModel.location = item.name)}
                containerStyle={{ padding: 5 }}
                textInputStyle={{
                  padding: 12,
                  borderWidth: 1,
                  borderColor: "#ccc",
                  backgroundColor: "#FAF7F6",
                  borderRadius: 5
                }}
                itemStyle={{
                  padding: 10,
                  marginTop: 2,
                  backgroundColor: "#FAF9F8",
                  borderColor: "#bbb"
                }}
                itemTextStyle={{
                  color: "#222"
                }}
                itemsContainerStyle={{
                  maxHeight: "100%"
                }}
                items={this.state.locationCollection}
                placeholder="Enter Location"
                resetValue={false}
                underlineColorAndroid="transparent"
              />

              {this.renderDateTimeDropdown()}

              {this.renderButton()}

              {this.renderErrorMessage()}
            </View>
          </View>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    paddingRight: 15,
    paddingLeft: 15
  },
  pdTop5: {
    paddingTop: 5
  },
  homeTabs: {
    backgroundColor: "#000000",
    paddingTop: 5,
    paddingRight: 15,
    paddingBottom: 15,
    paddingLeft: 15,
    width: "100%"
  },
  btnOrder: {
    backgroundColor: "#005697",
    borderColor: "#005697",
    borderRadius: 4,
    paddingTop: 6,
    paddingRight: 0,
    paddingBottom: 6,
    paddingLeft: 0,
    textAlign: "center",
    fontSize: 15.5,
    color: "#fff",
    marginTop: "1%",
    marginRight: "1%",
    marginBottom: "1%",
    marginLeft: "1%"
  },
  validationAlert: {
    backgroundColor: "#d9534f",
    textAlign: "center",
    fontSize: 16,
    padding: 15,
    fontWeight: "900",
    borderRadius: 4
  },
  btnPlaceOrder: {},
  btnCancelOrder: {
    backgroundColor: "red",
    borderColor: "#005697",
    borderRadius: 4,
    paddingTop: 6,
    paddingRight: 0,
    paddingBottom: 6,
    paddingLeft: 0,
    textAlign: "center",
    fontSize: 15.5,
    color: "#fff",
    marginTop: "1%",
    marginRight: "1%",
    marginBottom: "1%",
    marginLeft: "1%"
  }
});
